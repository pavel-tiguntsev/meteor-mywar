Accounts.onLogin(function () {
    FlowRouter.go('lobby')
})

FlowRouter.route('/', {
    name: 'home',
    title: "Welcome to myWar",
    action() {
        // GAnalytics.pageview();
         GAnalytics.pageview();
        BlazeLayout.render('main', {footer: "footer"});
    }
});

FlowRouter.route('/lobby', {
    name: 'lobby',
    title: "Лобби",
    action() {
        // GAnalytics.pageview();
         GAnalytics.pageview();
        if (Meteor.userId()) {
        BlazeLayout.render('lobby', {footer: "footer"}) } else {
            FlowRouter.go('home')
        }
    }
});

FlowRouter.route('/ref/:id', {
    name: 'ref',
    title: "Welcome to myWar",
    action: function(params) {
        // GAnalytics.pageview();
         GAnalytics.pageview();
        Session.set('referer', params.id);
        BlazeLayout.render('main');
        
    }
});
/*
Router.route('/datastore', function () {
    this.response.setHeader('access-control-allow-origin', '*');
  var req = this.request.body;
  var res = this.response;
  console.log('req', req);
  res.end('hello from the server\n');
}, {where: 'server'});

*/
