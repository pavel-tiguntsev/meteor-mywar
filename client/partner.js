Template.partner.onCreated(function(){
    var self = this;
    self.autorun(function(){
         self.subscribe('partner');
    });



});


Template.partner.helpers({
	salary: () =>  {
		if  ( Partner.findOne({user: Meteor.userId()}) == undefined) { return} else {
		var referals = Partner.findOne({user: Meteor.userId()}).referals
		var sum = 0
		for(var i = 0; i < referals.length; i++) {
			sum += referals[i][1]
		}
		return sum }
	},
	count: () =>  {
		if  ( Partner.findOne({user: Meteor.userId()}) == undefined) { return} else {return Partner.findOne({user: Meteor.userId()}).referals.length }
		
	},
})
