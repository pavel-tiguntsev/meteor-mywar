var myLogoutFunc = function() {
	FlowRouter.go('/');
}

var myLoginFunc = function() {
	FlowRouter.go('/lobby');
}
	
	


AccountsTemplates.configure({
	termsUrl: 'terms-of-use',
	privacyUrl: 'privacy',
	onLogoutHook: myLogoutFunc,
	onSubmitHook: myLoginFunc
});

