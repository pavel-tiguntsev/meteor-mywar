Template.lobby.events({
	'click #logout': ()=> {
		AccountsTemplates.logout();
	}
});

Template.lobby.onCreated(function(){
    var self = this;
    self.autorun(function(){
         self.subscribe('skills');
         self.subscribe('cashier');
		self.subscribe('games');
		self.subscribe('games2');
    });



});
Template.lobby.rendered = function () {
	if (Skills.findOne({user: Meteor.userId()}) == undefined) {
		return
	} else {
		console.log('asdasdasd', Skills.findOne({user: Meteor.userId()})['0'][0])
	}
	
};
Template.lobby.helpers({
	rockSC: () =>  {
		if  ( Skills.findOne({user: Meteor.userId()}) == undefined) { return} else { 
			var skill = Skills.findOne({user: Meteor.userId()})['0'][0];
			console.log(skill);

			var width = Math.max(1, skill/10000)+ "%";
			console.log(width);
			$("#rocklobby #skills #red").css({'width': width})
			; return skill}
		
	},
	rockPA: () =>  {
		if  ( Skills.findOne({user: Meteor.userId()}) == undefined) { return} else { 
			var skill = Skills.findOne({user: Meteor.userId()})['0'][1];
			console.log(skill);

			var width = Math.max(1, skill/10000)+ "%";
			console.log(width);
			$("#rocklobby #skills #yellow").css({'width': width})
			; return skill}
		
	},
	scissorsRO: () =>  {
		if  ( Skills.findOne({user: Meteor.userId()}) == undefined) { return} else { 
			var skill = Skills.findOne({user: Meteor.userId()})['1'][0];
			console.log(skill);

			var width = Math.max(1, skill/10000)+ "%";
			console.log(width);
			$("#cutterlobby #skills #blue").css({'width': width})
			; return skill}
	},
	scissorsPA: () =>  {
		if  ( Skills.findOne({user: Meteor.userId()}) == undefined) { return} else { 
			var skill = Skills.findOne({user: Meteor.userId()})['1'][1];
			console.log(skill);

			var width = Math.max(1, skill/10000)+ "%";
			console.log(width);
			$("#cutterlobby #skills #yellow").css({'width': width})
			; return skill}
	},
	paperRO: () =>  {
		if  ( Skills.findOne({user: Meteor.userId()}) == undefined) { return} else { 
			var skill = Skills.findOne({user: Meteor.userId()})['2'][0];
			console.log(skill);

			var width = Math.max(1, skill/10000)+ "%";
			console.log(width);
			$("#suitlobby #skills #blue").css({'width': width})
			; return skill}
	},
	paperSC: () =>  {
		if  ( Skills.findOne({user: Meteor.userId()}) == undefined) { return} else { 
			var skill = Skills.findOne({user: Meteor.userId()})['2'][1];
			console.log(skill);

			var width = Math.max(1, skill/10000)+ "%";
			console.log(width);
			$("#suitlobby #skills #red").css({'width': width})
			; return skill}
	},
	address: () => {
		if  (Meteor.user() == undefined) { return} else { return Meteor.user().emails[0].address }
		
	},
	ver: () => {
		if  (Meteor.user() == undefined) { return} else { return (!Meteor.user().emails[0].verified) }
		
	},
	balance: () => {
		if  ( Cashier.findOne({user: Meteor.userId()}) == undefined) { return} else { return Cashier.findOne({user: Meteor.userId()}).balance }
		
	},
	
	games: () => {
		return Games.find({waiting: true})
	},

})

Template.lobby.events({
  'submit #addbattle'(event) {
  	console.log('submit')
  	event.preventDefault();
    const target = event.target;
    const bet = target.bet.value;
    const warrior = target.warrior.value;
    const text = target.text.value;
    Meteor.call('newGame', parseInt(warrior), parseInt(bet), text, function (error, result) {if (error) { } else { } }) 
    target.bet.value = 100;


    
  }})