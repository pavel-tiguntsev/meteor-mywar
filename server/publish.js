Meteor.publish('games', function () {
  return Games.find({ creator: this.userId  });
});

Meteor.publish('games2', function () {
  return Games.find({}, {fields: {bet: 1, date: 1, text: 1, waiting: 1}});
});


Meteor.publish('gamelogs', function () {
  if (this.userId == undefined) {return} else {
  return GameLogs.find( { $or: [
	{ creator: {$in: [Meteor.users.findOne(this.userId).username]}},
  	{ player: {$in: [Meteor.users.findOne(this.userId).username]}}

  	]}

  	);
}});


  Meteor.publish('skills', function skills() {

    return Skills.find({user: this.userId});

  });

  
  Meteor.publish('cashier', function () {

    return Cashier.find({user: this.userId});

  });

    Meteor.publish('partner', function () {

    return Partner.find({user: this.userId});

  });

  /*
*/