
import { Meteor } from 'meteor/meteor';

var bodyParser = Npm.require('body-parser');

// Define our middleware using the Picker.middleware() method.
Picker.middleware( bodyParser.json() );
Picker.middleware( bodyParser.urlencoded( { extended: false } ) );



Picker.route('/test', function(params, request, response, next) {
  response.setHeader('access-control-allow-origin', '*');
  console.log( request.body );
  response.end();
}); 

Meteor.startup( function() {





// process.env.MAIL_URL = 'smtp://965081decdeb2833c8073b24ac612938:345237aba863df1f38beabf614ef362a@in-v3.mailjet.com:587/'
process.env.MAIL_URL = 'smtp://contact@mywar.ml:yujiku99@smtp.yandex.ru:25/'

/* WebApp.rawConnectHandlers.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  return next();
}); */
});

Meteor.methods({

    getIP: function(){
    	
    	if  (!this.isSimulation) {
        var ip = this.connection.clientAddress
    	console.log(ip)
        return ip;}
        
    },
    'newReferal'(refId) {
    	console.log('newReferal');
    	if (!Meteor.users.findOne(this.userId).emails.verified) {
    		throw new Meteor.Error('email-not-verified')
    	}
    	if (refId == undefined) {
    		Meteor.users.update(this.userId, {$set: {referer: 'no-ref'}})
    	}
    	if ((Meteor.users.findOne(refId) != undefined) && (!Meteor.users.findOne(this.userId).referer)) {
    	Meteor.users.update(this.userId, {$set: {referer: refId}})
    	Partner.update({user: refId}, {$addToSet: {referals: [Meteor.userId(), 500]}})
    	Cashier.update({user: refId}, {$inc: {balance: 500}})
    }
    },
	'newGame'(warrior, bet, text) {
		console.log('newGame');
		check(warrior, Number);
    	check(bet, Number);
    	console.log('pass')
		if (!Meteor.users.findOne(this.userId).emails[0].verified) {
    		throw new Meteor.Error('email-not-verified')
    	}
		

    	if (isNaN(Cashier.findOne({user: this.userId}).balance)) {
    		throw new Meteor.Error('balance-isNaN');
    	}
		if (bet > Cashier.findOne({user: this.userId}).balance) {
      		throw new Meteor.Error('low-balance');
    	}
    	
    	if (!((warrior == 0) || (warrior == 1) || (warrior == 2))) {
    		throw new Meteor.Error('not-a-warrior');
    	}
    	
    	if (bet < 100) {
    		throw new Meteor.Error('low-than-min');
    	}
    	Cashier.update({user: this.userId}, {$inc: {balance: -bet}})
		Games.insert({creator: this.userId, creatorWarrior: warrior, bet, text, date: new Date(), waiting: true})
	},
	'playGame'(gameId, warrior) {
		if (!Meteor.users.findOne(this.userId).emails[0].verified) {
    		throw new Meteor.Error('email-not-verified')
    	}
		

		if (isNaN(Cashier.findOne({user: this.userId}).balance)) {
    		throw new Meteor.Error('balance-isNaN');
    	}
		console.log('SecondPlayer...')
		if (Cashier.findOne({user: this.userId}).balance < Games.findOne(gameId).bet) {
			throw new Meteor.Error('low-balance');
		}
		if (!((warrior == 0) || (warrior == 1) || (warrior == 2))) {
    		throw new Meteor.Error('not-a-warrior');
    	}
		check(gameId, String);
		check(warrior, Number);
		console.log('all checked')
		Cashier.update({user: this.userId}, {$inc: {balance: -Games.findOne(gameId).bet}})
		console.log('-money');

		Games.update(gameId, {$set: {player: this.userId, secondWarrior: warrior, toPlay: true, waiting: false}});
		Meteor.call('getBankDevided', gameId);
	},
	'getBankDevided'(gameId) {
		if (!Games.findOne(gameId).toPlay) {
    		throw new Meteor.Error('this-not-for-play');
    	}
    	console.log("result...")
		var maxPower = 2000000;
		var bank = (Games.findOne(gameId).bet*2)*0.95;
		var userAid = Games.findOne(gameId).creator
		var userAwar = Games.findOne(gameId).creatorWarrior
		var userBid = Games.findOne(gameId).player
		var userBwar = Games.findOne(gameId).secondWarrior
	    var aGets = 0;
	    var bGets = 0;
	    var a = Skills.findOne({user: userAid})[userAwar.toString()];
	    var b = Skills.findOne({user: userBid})[userBwar.toString()];
	    if ((userAwar == 0) && (userBwar == 1)) {
	        bGets = b[1]/maxPower;
	        aGets = 1 - bGets;
	        
	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));
	        var newSkillA = a[0] + SkillAplus;
	        var newSkillB = b[1] + SkillBplus;

	        Skills.update({user: userAid}, {$set: {[userAwar]: [newSkillA, a[1]]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [b[0], newSkillB]}});

			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})
			Games.update(gameId, {$set: {toPlay: false}});
		    GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, 1], skillB: [SkillBplus, 0]})
		    return;
	    }
	    if (userAwar == userBwar) {
	        console.log("(a[0] == b[0])")
	        if (userAwar > 0) {
	        	var contr = userAwar - 1
	            bGets = b[contr]/ (a[contr] + b[contr]);
	            aGets = a[contr] / (a[contr] + b[contr]);

	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));

	        if (userAwar == 1) {
	     	var newSkillA = a[0] + SkillAplus;
	        var newSkillB = b[0] + SkillBplus;
	        Skills.update({user: userAid}, {$set: {[userAwar]: [newSkillA, a[1]]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [newSkillB, b[1]]}});
	    	} else {
		     	var newSkillA = a[1] + SkillAplus;
				var newSkillB = b[1] + SkillBplus;
	    		Skills.update({user: userAid}, {$set: {[userAwar]: [a[0],newSkillA]}});
	        	Skills.update({user: userBid}, {$set: {[userBwar]: [b[0],newSkillB]}});
	    	}
			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})
			GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, contr], skillB: [SkillBplus, contr]})
			Games.update(gameId, {$set: {toPlay: false}});
		    console.log([[aGets*bank, SkillAplus],[bGets*bank, (bank*0.05 - SkillAplus)]])
	        } else {
	            bGets = b[1]/(a[1] + b[1]);
	            aGets = 1 - bGets;
	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));
	        var newSkillA = a[1] + SkillAplus;
	        var newSkillB = b[1] + SkillBplus;

	        Skills.update({user: userAid}, {$set: {[userAwar]: [a[0], newSkillA]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [b[0], newSkillB]}});

			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})

			Games.update(gameId, {$set: {toPlay: false}});
		    console.log([[aGets*bank, SkillAplus],[bGets*bank, (bank*0.05 - SkillAplus)]])
		    GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, 2], skillB: [SkillBplus, 2]})
		    return;
	        }
	    }
	    if ((userAwar == 0) && (userBwar == 2)) {
	        console.log("((a[0] == 0) && (b[0] == 2))")
	        aGets = a[0]/maxPower;
	        bGets = 1 - aGets;
	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));
	        var newSkillA = a[0] + SkillAplus
	        var newSkillB = b[0] + SkillBplus

	        Skills.update({user: userAid}, {$set: {[userAwar]: [newSkillA, a[1]]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [newSkillB, b[1]]}});

			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})

			Games.update(gameId, {$set: {toPlay: false}});
		    console.log([[aGets*bank, SkillAplus],[bGets*bank, (bank*0.05 - SkillAplus)]])
		    GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, 1], skillB: [SkillBplus, 0]})
		    return;
	    }
	    if ((userAwar == 1) && (userBwar == 0)) {
	        console.log("((a[0] == 1) && (b[0] == 0))")
	        aGets = a[1]/maxPower;
	        bGets = 1 - aGets;
	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));
	        var newSkillA = a[1] + SkillAplus
	        var newSkillB = b[0] + SkillBplus

	        Skills.update({user: userAid}, {$set: {[userAwar]: [a[0], newSkillA]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [newSkillB, b[1]]}});

			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})
 GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, 1], skillB: [SkillBplus, 0]})
			Games.update(gameId, {$set: {toPlay: false}});
		    console.log([[aGets*bank, SkillAplus],[bGets*bank, (bank*0.05 - SkillAplus)]])
	    }
	    if ((userAwar == 1) && (userBwar == 2)) {
	        console.log("((a[0] == 1) && (b[0] == 2))")
	        bGets = b[0]/maxPower;
	        aGets = 1 - aGets;

	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));
	        var newSkillA = a[1] + SkillAplus;
	        var newSkillB = b[0] + SkillBplus;

	        Skills.update({user: userAid}, {$set: {[userAwar]: [a[1], newSkillA]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [newSkillB, b[0]]}});

			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})
 GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, 1], skillB: [SkillBplus, 0]})
			Games.update(gameId, {$set: {toPlay: false}});
		    console.log([[aGets*bank, SkillAplus],[bGets*bank, (bank*0.05 - SkillAplus)]])
	    }
	    if ((userAwar == 2) && (userBwar == 0)) {
	        console.log("((a[0] == 2) && (b[0] == 0))")
	        bGets = b[0]/maxPower;
	        aGets = 1 - aGets;
	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));
	        var newSkillA = a[0] + SkillAplus
	        var newSkillB = b[0] + SkillBplus

	        Skills.update({user: userAid}, {$set: {[userAwar]: [newSkillA, a[1]]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [newSkillB, b[1]]}});

			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})
 GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, 1], skillB: [SkillBplus, 0]})
			Games.update(gameId, {$set: {toPlay: false}});
		    console.log([[aGets*bank, SkillAplus],[bGets*bank, (bank*0.05 - SkillAplus)]])
	    }
	    if ((userAwar == 2) && (userBwar == 1)) {
	        console.log("((a[0] == 2) && (b[0] == 1))")
	        aGets = a[0]/maxPower;
	        bGets = 1 - aGets;
	        var SkillAplus = parseFloat((aGets*(bank*0.05)).toFixed(2));
	        var SkillBplus = parseFloat((bGets*(bank*0.05)).toFixed(2));
	        var newSkillA = a[0] + SkillAplus
	        var newSkillB = b[1] + SkillBplus

	        Skills.update({user: userAid}, {$set: {[userAwar]: [newSkillA, a[1]]}});
	        Skills.update({user: userBid}, {$set: {[userBwar]: [b[0], newSkillB]}});

			Cashier.update({user: userAid}, {$inc: {balance: parseFloat((aGets*bank).toFixed(2))}})
			Cashier.update({user: userBid}, {$inc: {balance: parseFloat((bGets*bank).toFixed(2))}})
 GameLogs.insert({ date: new  Date(), gameId, bank: Games.findOne(gameId).bet * 2, creator: [Meteor.users.findOne(userAid).username, userAwar, aGets*bank], player: [Meteor.users.findOne(userBid).username, userBwar, bGets*bank], skillA: [SkillAplus, 1], skillB: [SkillBplus, 0]})
			Games.update(gameId, {$set: {toPlay: false}});
		    console.log([[aGets*bank, SkillAplus],[bGets*bank, (bank*0.05 - SkillAplus)]])
	    }

	},
})