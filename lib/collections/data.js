Games = new Mongo.Collection('games');
Skills = new Mongo.Collection('skills');
Cashier = new Mongo.Collection('cashier');
Partner = new Mongo.Collection('partner');
GameLogs = new Mongo.Collection('gamelogs');




Games.allow({
    insert: function(userId, doc) {return !!this.userId}})

Meteor.users.deny({
  update() { return true; }
});
	Skills.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});


Cashier.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Partner.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

GameLogs.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
