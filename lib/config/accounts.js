var myLogoutFunc = function() {
  FlowRouter.go('/');
};

var myPostSubmitFunc = function() {
  FlowRouter.go('/lobby');
}

AccountsTemplates.configure({
    // Behavior
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: false,
    overrideLoginErrors: true,
    sendVerificationEmail: true,
    lowercaseUsername: false,
    focusFirstInput: true,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: true,
    showLabels: false,
    showPlaceholders: true,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    homeRoutePath: '/',
    redirectTimeout: 4000,




});

AccountsTemplates.addFields([
    {
        _id: 'referer',
        required: false,
        type: 'hidden'
    },
    {
      _id: "username",
      type: "text",
      displayName: "{{_'username'}}",
      required: true,
      minLength: 4,
      re: /^[a-zA-Z0-9]+$/
  }

]);

